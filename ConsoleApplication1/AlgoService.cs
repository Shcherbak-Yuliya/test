﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public static class CalculateService 
    {    
        /// <summary>
        /// Method calculate average for each currency first and then average between sources
        /// </summary>
        public static void CalculateMode1(DateTime start, DateTime end) 
        {
            var firstSource = DataReader.readXML(start, end);
            var secondSource = DataReader.readCSV(start, end);

            List<CurrencyModel> list = new List<CurrencyModel>();

            foreach (var rate in firstSource)
            {
                list.Add(new CurrencyModel
                {
                    Currency = rate.Key,
                    Rate = (rate.Value.Average() + secondSource[rate.Key].Average()) / 2
                });
            }

            //return s;
        }

        /// <summary>
        /// Method calculate average for all rates of the sources
        /// </summary>
        public static void CalculateMode2(DateTime start, DateTime end) 
        {
            var firstSource = DataReader.readXML(start, end);
            var secondSource = DataReader.readCSV(start, end);
            List<CurrencyModel> list = new List<CurrencyModel>();
            foreach (var rate in firstSource)
            {
                rate.Value.AddRange(secondSource[rate.Key]);
                list.Add(new CurrencyModel
                {
                    Currency = rate.Key,
                    Rate = rate.Value.Average()
                });                   
            }
                         
            //return s;
        }
    }
}
